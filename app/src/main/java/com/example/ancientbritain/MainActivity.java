package com.example.ancientbritain;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {
   // private RecyclerView recyclerView;
    public static int currentItem;

    //To response to RecyclerView Events
    static View.OnClickListener mainOnClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mainOnClickListener = new MainOnClickListener(this);

        //recyclerView = (RecyclerView) findViewById(R.id.main_recycler_view);
        //recyclerView.setHasFixedSize(true);
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        //recyclerView.setLayoutManager(layoutManager);
        DownloadJSONTask downloadJSONTask = new DownloadJSONTask();
        downloadJSONTask.execute("http://192.168.1.4:8000/MDC/list");
        Log.i("MainActiv", "We have comeback to Main");
        MainData mainData = MainData.getProductArrayInstance();
        Log.i("MainActiv", String.valueOf(mainData.getProducts().size()));
        //This code is for testing code only u could remove it after testing
        for(int i = 0;i < mainData.getProducts().size();i++){
            Log.i("MainActiv", mainData.getProducts().get(i).getPName());
            Log.i("MainActiv", mainData.getProducts().get(i).getPDesc());
        }
        //End testing
        //RecyclerView.Adapter adapter = new MainAdapter(mainData);
        //recyclerView.setAdapter(adapter);
    }
/*
    private class MainOnClickListener implements View.OnClickListener{
        private final Context context;
        private MainOnClickListener(Context c){
            this.context = c;
        }
        @Override
        public void onClick(View v){
            currentItem = recyclerView.getChildAdapterPosition(v);
            //The below code is for testing purpose only, u can remove it after testing
            Log.i("MainActivity", String.valueOf(currentItem));
            startActivity(new Intent(getApplicationContext(), DetailActivity.class));
        }
    }
    */

}
