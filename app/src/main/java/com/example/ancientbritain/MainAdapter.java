package com.example.ancientbritain;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {
    MainData mainData;
    public MainAdapter(MainData mainData){
        mainData = MainData.getProductArrayInstance();
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_main, viewGroup,
                false);
        view.setOnClickListener(MainActivity.mainOnClickListener);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainViewHolder mainViewHolder, int i) {
        ImageView imageIcon = mainViewHolder.imageIcon;
        TextView textName = mainViewHolder.textName;
        TextView textInfo = mainViewHolder.textInfo;
        //Log.i("MainAdapter",Integer.toString(mainData.get(i).getImage()));

        //imageIcon.setImageResource(mainData.get(i).getImage());
        textName.setText(mainData.getProducts().get(i).getPName());
        textInfo.setText(mainData.getProducts().get(i).getPDesc());
    }

    @Override
    public int getItemCount() {
        int itemCount;
        try{
            itemCount = mainData.getProducts().size();
        }catch (NullPointerException e){
            Log.e("MainAdapter", e.toString());
            itemCount = -1;
        }
        return itemCount;
    }

    public static class MainViewHolder extends RecyclerView.ViewHolder{
        ImageView imageIcon;
        TextView textName;
        TextView textInfo;


        public MainViewHolder(View itemView) {
            super(itemView);
            this.imageIcon = (ImageView) itemView.findViewById(R.id.card_image);
            this.textName = (TextView) itemView.findViewById(R.id.card_name);
            this.textInfo = (TextView) itemView.findViewById(R.id.card_info);

        }
    }


}
