package com.example.ancientbritain;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadJSONTask extends AsyncTask<String, Void, String> {
    MainData productArray;


    public DownloadJSONTask(){
        productArray = MainData.getProductArrayInstance();
    }

    @Override
    protected void onPreExecute(){

    }

    @Override
    protected String doInBackground(String... urls){
        InputStream stream = null;
        HttpURLConnection connection = null;
        String result = null;
        try{
            URL url = new URL(urls[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(3000);
            connection.setConnectTimeout(3000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if(responseCode != HttpURLConnection.HTTP_OK){
                throw new IOException("HTTP Error connection" + responseCode);
            }
            stream = connection.getInputStream();
            if(stream != null){
                result = readStream(stream);
            }
            else{
                Log.e("doInBackground", "NULL response");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }


    @Override
    protected void onPostExecute(String result){
        jsonDeserializer(result);
    }


    //Convert the downloaded data into String
    private String readStream(InputStream stream) throws IOException, UnsupportedEncodingException {
        //Put our data into a buffer
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String readLine;
        StringBuffer stringBuffer = new StringBuffer();
        while((readLine = reader.readLine()) != null){
            stringBuffer.append(readLine);
        }
        Log.i("Result", stringBuffer.toString());
        return stringBuffer.toString();

    }


    //Deserialize JSON String and put it in MainData Array
    private void jsonDeserializer(String json_string){
        try{
            JSONArray jsonArray = new JSONArray(json_string);
            for(int i = 0;i< jsonArray.length();i++){

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                MainDataDef product = new MainDataDef(jsonObject.getString("product_name"),
                        jsonObject.getString("product_description"));
                productArray.addToArray(product);
                product = null;
                Log.i("jsonDeserializer", "DONE");
                testArray();
            }

        }catch (JSONException e){
            Log.e("JSONException", e.getMessage());
            Log.e("JSONException", e.getCause().toString());
        }

    }


    private void testArray(){
        //This code is for testing code only u could remove it after testing
        for(int i = 0;i < productArray.getProducts().size();i++){
            Log.i("jsonDeserializer", productArray.getProducts().get(i).getPName());
            Log.i("jsonDeserializer", productArray.getProducts().get(i).getPDesc());
        }
        //End testing

    }


}
