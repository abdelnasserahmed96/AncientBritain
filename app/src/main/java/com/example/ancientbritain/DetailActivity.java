package com.example.ancientbritain;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class DetailActivity extends AppCompatActivity {
    private ImageView detailImage;
    private ArrayList<MainDataDef> detailData;


/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        detailImage = (ImageView) findViewById(R.id.detail_image);
        TextView detailName = (TextView) findViewById(R.id.detail_name);
        TextView detailDistance = (TextView) findViewById(R.id.detail_distance);
        TextView detailText = (TextView) findViewById(R.id.detail_distance);
        ImageView detailWebLink = (ImageView) findViewById(R.id.detail_web_link);
        //Apply the scroll bar to the detailText
        detailText.setMovementMethod(new ScrollingMovementMethod());

        //Link views to data
        int i = MainActivity.currentItem;
        //Next two lines are to get random distance
        Random n = new Random();
        int m = n.nextInt((600 - 20) + 1) + 20;
        setTitle(getString(R.string.app_name) + " - " + MainData.nameArray[i]);
        detailImage.setImageResource(MainData.detailImageArray[i]);
        detailDistance.setText(String.valueOf(m) + " miles");
        detailText.setText(MainData.detailTextArray[i]);

        //Program the web link to open the browser when clicking on it
        detailWebLink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(MainData.detailWebLink[MainActivity.currentItem]));
                startActivity(intent);
            }
        });


    }
    */
}
