package com.example.ancientbritain;

public class ProductArray {
    private static final ProductArray ourInstance = new ProductArray();

    public static ProductArray getInstance() {
        return ourInstance;
    }

    private ProductArray() {
    }
}
