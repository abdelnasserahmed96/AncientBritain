package com.example.ancientbritain;

public class MainDataDef {

    private String product_name;
    private String product_desc;
    private float product_price;


    public MainDataDef(){

    }

    public MainDataDef(String pName, String pDesc){
        this.product_name = pName;
        this.product_desc = pDesc;
    }


    public String getPName(){
        return product_name;
    }
    public String getPDesc(){
        return product_desc;
    }

    public float getPPrice(){
        return product_price;
    }



}
